package org.neoflex.megacryptoapplicationbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MegaCryptoApplicationBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MegaCryptoApplicationBackendApplication.class, args);
    }

}
